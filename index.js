// Selecting DOM elements
const numbers = document.querySelectorAll('.numbers');
const result = document.querySelector('.result span');
const signs = document.querySelectorAll('.sign');
const equals = document.querySelector('.equals');
const clear = document.querySelector('.clear');
const negative = document.querySelector('.negative');
const percent = document.querySelector('.percent');

// Global variables for calculator state
let firstValue = "";
let isFirstValue = false;
let secondValue = "";
let isSecondValue = false;
let sign = "";
let resultValue = 0;

// Event listeners for numeric buttons
for (let i = 0; i < numbers.length; i++) {
  numbers[i].addEventListener('click', (e) => {
    let atr = e.target.getAttribute('value');
    // If it's the first value, update firstValue
    if (isFirstValue === false) {
      getFirstValue(atr);
    }
    // If it's the second value, update secondValue
    if (isSecondValue === false) {
      getSecondValue(atr);
    }
  });
}

// Function to handle the click event for numeric buttons
function getFirstValue(el) {
  // Clear the result display
  result.innerHTML = "";
  // Update firstValue and display it
  firstValue += el;
  result.innerHTML = firstValue;
  // Convert firstValue to a number
  firstValue = +firstValue;
}

// Function to handle the click event for operator buttons
function getSecondValue(el) {
  // Check if the firstValue and sign are set
  if (firstValue !== "" && sign !== "") {
    // Update secondValue and display it
    secondValue += el;
    result.innerHTML = secondValue;
    // Convert secondValue to a number
    secondValue = +secondValue;
  }
}

// Function to handle the click event for operator buttons
function getSign() {
  for (let i = 0; i < signs.length; i++) {
    signs[i].addEventListener('click', (e) => {
      // Get the operator value and set isFirstValue to true
      sign = e.target.getAttribute('value');
      isFirstValue = true;
    });
  }
}

// Initialize the event listener for operator buttons
getSign();

// Event listener for the equals button
equals.addEventListener('click', () => {
  result.innerHTML = "";
  // Perform the calculation based on the operator
  if (sign === "+") {
    resultValue = add(firstValue, secondValue);
  } else if (sign === "-") {
    resultValue = subtract(firstValue, secondValue);
  } else if (sign === "x") {
    resultValue = multiply(firstValue, secondValue);
  } else if (sign === "/") {
    resultValue = divide(firstValue, secondValue);
  }
  // Display the result and update global variables
  result.innerHTML = resultValue;
  firstValue = resultValue;
  secondValue = "";
  // Check and format the result length
  checkResultLength();
});

// Functions for basic arithmetic operations
function add(a, b) {
  return a + b;
}

function subtract(a, b) {
  return a - b;
}

function multiply(a, b) {
  return a * b;
}

function divide(a, b) {
  // Check for division by zero
  if (b === 0) {
    result.innerHTML = "Error: Division by zero";
    return undefined;
  }
  return a / b;
}

// Function to check and format the result length
function checkResultLength() {
  // Convert resultValue to a string
  resultValue = JSON.stringify(resultValue);
  // Check if the length is greater than or equal to 8
  if (resultValue.length >= 8) {
    // Parse and format the result with 5 decimal places
    resultValue = JSON.parse(resultValue);
    result.innerHTML = resultValue.toFixed(5);
  }
}

// Event listener for the negation button
negative.addEventListener('click', () => {
  result.innerHTML = "";
  // Negate the firstValue if it's not empty
  if (firstValue !== "") {
    resultValue = -firstValue;
    firstValue = resultValue;
  }
  // Negate the resultValue if both firstValue, secondValue, and sign are set
  if (firstValue !== "" && secondValue !== "" && sign !== "") {
    resultValue = -resultValue;
  }
  // Display the result
  result.innerHTML = resultValue;
});

// Event listener for the percentage button
percent.addEventListener('click', () => {
  result.innerHTML = "";
  // Calculate the percentage of the firstValue if it's not empty
  if (firstValue !== "") {
    resultValue = firstValue / 100;
    firstValue = resultValue;
  }
  // Calculate the percentage of the resultValue if both firstValue, secondValue, and sign are set
  if (firstValue !== "" && secondValue !== "" && sign !== "") {
    resultValue = resultValue / 100;
  }
  // Display the result
  result.innerHTML = resultValue;
});

// Event listener for the clear button
clear.addEventListener('click', () => {
  result.innerHTML = "";
  // Reset all global variables
  firstValue = "";
  isFirstValue = false;
  secondValue = "";
  isSecondValue = false;
  sign = "";
  resultValue = 0;
});
